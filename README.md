# car-dealer
Car dealer, Java Terminal

## How to compile?
```bash
# Go into the project source folder (src)
$ cd car-dealer/src

# Compile the main class
$ javac net/xeill/elpuig/Main.java

# Run the main
$ java net.xeill.elpuig.Main
```
