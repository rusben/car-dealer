package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the Messages
*
* @author ASIX1
*
*/

public class Messages {

  public void mainMenu() {
    System.out.println("--------------------------------------------------------------------------------");
    System.out.println("-----            Welcome to the best Car Dealer in Santa Coloma            -----");
    System.out.println("-----  =====     =====     =====     =====     =====     =====     =====   -----");
    System.out.println("-----  1 - Show catalog                                                    -----");
    System.out.println("-----  2 - Add a new car                                                   -----");
    System.out.println("-----  3 - Remove a new car                                                -----");
    System.out.println("-----  4 - Buy a car                                                       -----");
    System.out.println("-----  5 - Save catalog                                                    -----");
    System.out.println("-----  6 - Save and exit                                                   -----");
    System.out.println("-----  7 - Close without saving                                            -----");
    System.out.println("-----  =====     =====     =====     =====     =====     =====     =====   -----");

  }

}
