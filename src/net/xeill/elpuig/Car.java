package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the Car
*
* @author ASIX1
*
*/

public class Car {

  String id;
  String brand;
  String model;
  float price;
  int seats;
  float weight;

  Car(String id, String brand, String model, float price, int seats, float weight) {
    this.id = id;
    this.brand = brand;
    this.model = model;
    this.price = price;
    this.seats = seats;
    this.weight = weight;
  }

}
